import subprocess
import re
import os


def mem_size():
  p = subprocess.Popen("""free -kt | grep '^Mem' | awk '{printf ("%lu", $2 * 0.5);}'""", shell=True, stdout=subprocess.PIPE)
  ret = p.stdout.read()
  r = p.wait()
  assert r == 0
  return ret

def run_with_limit(limit, cmd):
  p = subprocess.Popen("ulimit -v %s; ulimit -m %s; %s" % (limit, limit, cmd),
                       shell=True)
  p.wait()


class RunException(Exception):
  pass

def run_(cmd, stdout = subprocess.PIPE, stdin = None):
  p = subprocess.Popen(cmd, stdout = stdout, stdin = stdin)
  if p.stdout:
    d = p.stdout.read()
  else:
    d = None
  status = p.wait()
  return (status, d)

def run(cmd, stdout = subprocess.PIPE, stdin = None):
  status, ret = run_(cmd, stdout, stdin)
  if status != 0:
    raise RunException()
  return ret

def find_sums(directory):
  d = run(['find', directory, '-name', '*.sum'])
  d = [x.strip() for x in d.strip().split('\n') if x != '']
  exp = re.compile(directory + '(.*)')
  d = [exp.match(x).groups()[0] for x in d]
  return set(d)

def git_branch():
  t =run(['git', 'branch'])
  branch_line = [x for x in t.strip().splitlines() if x.startswith('*')][0]
  branch = branch_line[1:].strip()
  if branch == '(no branch)':
    branch = 'none'
  return branch

def parse_svn_rev(data):
  line = [x for x in data.splitlines() if 'git-svn-id' in x][0]
  parse = re.compile('.*@(\d*).*')
  return parse.match(line).groups()[0]

def parse_svn_branch(data):
  line = [x for x in data.splitlines() if 'git-svn-id' in x][0]
  parse = re.compile('.*/(.*)@.*')
  return parse.match(line).groups()[0]

def repo_status(src):
  cur = os.getcwd()
  os.chdir(src)
  t = run(['git', 'show', 'HEAD'])
  branch = git_branch()
  pristine = ['lto', 'lto-streamer', 'master', 'tuples', 'plugins']
  if 'git-svn-id' in t and branch in pristine:
    commit = parse_svn_rev(t)
    branch = parse_svn_branch(t)
  else:
    commit = t.splitlines()[0].split(' ')[1]
  os.chdir(cur)
  return commit, branch

def build_dir_(src):
  if os.path.exists(src + '/.svn'):
    return src + '-build'
  commit, branch = repo_status(src)
  return src + '-' + branch + '-' + commit

def build_dir(src, debug = False):
  d = build_dir_(src)
  if debug:
    return d + '-debug'
  return d
