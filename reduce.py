#!/usr/bin/python

import argparse
import common
import shutil
import subprocess

debug=0

def write_all(fname, l):
  f = open(fname, 'bw')
  f.write(b''.join(l))
  f.close()

def clean(fname):
  l = [x for x in open(fname, 'rb').readlines() if not x.startswith(b'# ') and x.strip() != b'']
  write_all(fname, l)

def topformflat(fname, level, write = False):
  clean(fname)
  data = common.run(['topformflat', str(level)], stdin=open(fname))
  lines = data.splitlines()
  lines = [x.strip() + b'\n' for x in lines if x.strip() != b'']
  if (write):
    write_all(fname, lines)
  return len(lines)

def count_lines_at_level(files, level):
  return sum(topformflat(f, level) for f in files)

def count_lines(files):
  lines = {}
  level = 0
  while True:
    t = count_lines_at_level(files, level)
    if level != 0 and lines[level - 1] == t:
      break
    lines[level] = t
    level = level + 1
  return lines

def run_delta(check, filename):
  #avoid empty files
  if not open(filename, 'rb').readlines():
    return

  common.run([check, filename])

  delta = common.run(['which', 'delta']).strip()
  cmd = ['perl', delta, '-in_place', '-test=%s' % check, filename]
  common.run(cmd, stdout = None)

  common.run([check, filename])

def run_multidelta(check, level, files):
  for f in files:
    shutil.copy(f, f + '-orig')
    print('file %s at level %s' % (f, level))
    topformflat (f, level, True)
    common.run([check, f], stdout = None)

  for f in files:
    run_delta (check, f);

def reduce_test(check, files, global_max_level):
  level = 0
  log = open('reduce.log', 'w')
  lines = count_lines(files)

  while True:

    log.write('%s: lines %s\n' % (level, lines))
    log.flush()

    run_multidelta(check, level, files)

    new_lines = count_lines(files)

    log.write('%s: new_lines %s\n' % (level, new_lines))
    log.flush()

    if lines != new_lines:
      level = 0
    else:
      max_level = min(max(new_lines.keys()), global_max_level)
      if level < max_level:
        level = level + 1
      else:
        break

    lines = new_lines

  log.close()

parser = argparse.ArgumentParser(description='reduce testcase.')
parser.add_argument('--max-level', type=int, help='max topformflat level', default=1000)
parser.add_argument('test', help='test program')
parser.add_argument('files', nargs='+', help='files to reduce')
args = parser.parse_args()
reduce_test(args.test, args.files, args.max_level)
